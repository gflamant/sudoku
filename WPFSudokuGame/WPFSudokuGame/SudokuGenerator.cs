﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WPFSudokuGame
{
    /// <summary>
    /// Classe de génération de Sudoku
    /// Format des grilles :
    /// 
    /// 9*9
    /// 16*16
    /// 25*25
    /// </summary>
    class SudokuGenerator
    {
        Grille generatedGrille;

        /// <summary>
        /// Initialisation d'une grille (qui sera à complété avec une méthode generateSudokuFormatX
        /// </summary>
        public SudokuGenerator()
        {
            generatedGrille = new Grille();
        }

        /// <summary>
        /// Génération d'une grille format 16*16
        /// </summary>
        /// <returns></returns>
        public Grille generateSudokuFormat16()
        {
            try
            {
                generatedGrille.Nom = "Généré " + App.ViewModelSudoku.GeneratedGrille;
                App.ViewModelSudoku.GeneratedGrille++;
                generatedGrille.isCompleted = false;
                generatedGrille.Date = DateTime.Now.ToString("dd/mm/yyyy hh:mm");
                generatedGrille.IsStarted = false;
                generatedGrille.IsValid = false;
                Case[,] tmp = new Case[16, 16];

                string filepath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\SudokuModel\16\sudoku16.sud";
                string[] lines = File.ReadAllLines(filepath);

                int l = 0;
                int c = 0;
                foreach (var item in lines)
                {
                    if (l == 0)
                        this.generatedGrille.Symboles = item;
                    else
                    {
                        foreach (char cValue in item)
                        {
                            var newCase = new Case(cValue, this.generatedGrille.Symboles);
                            tmp[l - 1, c] = newCase;

                            //Si la grille contient déjà le carré X (déterminé par la méthode square number) on ajoute la case dedans
                            if (generatedGrille.Squares.ContainsKey(squareNumber(l - 1, c, 25)))
                            {
                                Square square = generatedGrille.Squares[squareNumber(l - 1, c, 25)];
                                newCase.SquareNumber = squareNumber(l - 1, c, 25);
                                square.Cases.Add((l - 1).ToString() + c.ToString(), newCase);
                            }
                            //Sinon on créé le carré
                            else
                            {
                                Square square = new Square(9);
                                newCase.SquareNumber = squareNumber(l - 1, c, 25);
                                square.Cases.Add((l - 1).ToString() + c.ToString(), newCase);
                                generatedGrille.Squares.Add(squareNumber(l - 1, c, 25), square);
                            }
                            c++;
                        }
                        c = 0;
                    }
                    l++;
                }
                generatedGrille.Tab = tmp;
                blankRandomCase();
                generatedGrille.Tab = generatedGrille.Tab;
            }
            catch (Exception)
            {
                MessageBox.Show("Une erreur est survenue lors de la génération du Sudoku.", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
            return generatedGrille;
        }

        /// <summary>
        /// Génération d'une grille format 25
        /// </summary>
        /// <returns></returns>
        public Grille generateSudokuFormat25()
        {
            try
            {
                generatedGrille.Nom = "Généré " + App.ViewModelSudoku.GeneratedGrille;
                App.ViewModelSudoku.GeneratedGrille++;
                generatedGrille.isCompleted = false;
                generatedGrille.Date = DateTime.Now.ToString("dd/mm/yyyy hh:mm");
                generatedGrille.IsStarted = false;
                generatedGrille.IsValid = false;
                Case[,] tmp = new Case[25, 25];

                string filepath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\SudokuModel\25\sudoku25.sud";
                string[] lines = File.ReadAllLines(filepath);

                int l = 0;
                int c = 0;
                foreach (var item in lines)
                {
                    if (l == 0)
                        this.generatedGrille.Symboles = item;
                    else
                    {
                        foreach (char cValue in item)
                        {
                            var newCase = new Case(cValue, this.generatedGrille.Symboles);
                            tmp[l - 1, c] = newCase;

                            //Si la grille contient déjà le carré X (déterminé par la méthode square number) on ajoute la case dedans
                            if (generatedGrille.Squares.ContainsKey(squareNumber(l - 1, c, 25)))
                            {
                                Square square = generatedGrille.Squares[squareNumber(l - 1, c, 25)];
                                newCase.SquareNumber = squareNumber(l - 1, c, 25);
                                square.Cases.Add((l - 1).ToString() + c.ToString(), newCase);
                            }
                            //Sinon on créé le carré
                            else
                            {
                                Square square = new Square(9);
                                newCase.SquareNumber = squareNumber(l - 1, c, 25);
                                square.Cases.Add((l - 1).ToString() + c.ToString(), newCase);
                                generatedGrille.Squares.Add(squareNumber(l - 1, c, 25), square);
                            }
                            c++;
                        }
                        c = 0;
                    }
                    l++;
                }
                generatedGrille.Tab = tmp;
                blankRandomCase();
                generatedGrille.Tab = generatedGrille.Tab;
            }
            catch(Exception)
            {
                MessageBox.Show("Une erreur est survenue lors de la génération du Sudoku.", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
            return generatedGrille;
        }

        /// <summary>
        /// Génération d'une grille format 9*9
        /// </summary>
        /// <returns></returns>
        public Grille generateSudokuFormat9()
        {
            generatedGrille.Nom = "Généré " + App.ViewModelSudoku.GeneratedGrille;
            App.ViewModelSudoku.GeneratedGrille++;
            generatedGrille.isCompleted = false;
            generatedGrille.Date = DateTime.Now.ToString("dd/MM/yyyy hh:mm");
            generatedGrille.IsStarted = false;
            generatedGrille.Symboles = "123456789";
            generatedGrille.IsValid = false;
            Case [,] tmp = new Case[9,9];

            for (int i = 0; i < 9; i++)
			{
                for (int j = 0; j < 9; j++)
                {
                    var newCase = new Case('.', "123456789");
                    tmp[i, j] = newCase;

                    //Si la grille contient déjà le carré X (déterminé par la méthode square number) on ajoute la case dedans
                    if (generatedGrille.Squares.ContainsKey(squareNumber(i, j, 9)))
                    {
                        Square square = generatedGrille.Squares[squareNumber(i, j, 9)];
                        newCase.SquareNumber = squareNumber(i, j, 9);
                        square.Cases.Add(i.ToString() + j.ToString(), newCase);
                    }
                    //Sinon on créé le carré
                    else
                    {
                        Square square = new Square(9);
                        newCase.SquareNumber = squareNumber(i, j, 9);
                        square.Cases.Add(i.ToString() + j.ToString(), newCase);
                        generatedGrille.Squares.Add(squareNumber(i, j, 9), square);
                    }
                }
            }

            generatedGrille.Tab = tmp;
            assignRandomCase();
            new SudokuSolver().estValide(this.generatedGrille, 0);
            blankRandomCase();
            generatedGrille.Tab = generatedGrille.Tab;
            generatedGrille.IsStarted = false;
            generatedGrille.IsValid = false;
            return generatedGrille;
        }

        /// <summary>
        /// Enlève des cases
        /// </summary>
        private void blankRandomCase()
        {
            Random rand = new Random();
            int nbCaseToAssignBlank = ((int)Math.Pow(this.generatedGrille.Taille, 2)/4);
            int assignedCase = 0;

            while (assignedCase != nbCaseToAssignBlank)
            {
                var x = this.generatedGrille.Tab[rand.Next(this.generatedGrille.Taille),rand.Next(this.generatedGrille.Taille)];
                if(x.ValeurCase != '.')
                {
                    assignedCase++;
                    x.ValeurCase = '.';
                }
            }
        }

        /// <summary>
        /// Assignation de cases pour générer une solution.
        /// </summary>
        private void assignRandomCase()
        {
            Random rand = new Random();
            this.generatedGrille.Tab[0, 0].ValeurCase = rand.Next(9).ToString()[0];
            this.generatedGrille.Tab[1, 8].ValeurCase = rand.Next(9).ToString()[0];
            this.generatedGrille.Tab[2, 5].ValeurCase = rand.Next(9).ToString()[0];
            this.generatedGrille.Tab[3, 6].ValeurCase = rand.Next(9).ToString()[0];
            this.generatedGrille.Tab[4, 4].ValeurCase = rand.Next(9).ToString()[0];
            this.generatedGrille.Tab[5, 2].ValeurCase = rand.Next(9).ToString()[0];
            this.generatedGrille.Tab[6, 3].ValeurCase = rand.Next(9).ToString()[0];
            this.generatedGrille.Tab[7, 7].ValeurCase = rand.Next(9).ToString()[0];
            this.generatedGrille.Tab[8, 1].ValeurCase = rand.Next(9).ToString()[0];
        }

        /// <summary>
        /// Détermine le numéro du square
        /// </summary>
        /// <param name="row">Ligne</param>
        /// <param name="col">Colonne</param>
        /// <param name="width">Taille du Sudoku</param>
        /// <returns></returns>
        private static int squareNumber(int row, int col, int width)
        {
            int pos = ((row / (int)Math.Sqrt(width)) * (int)Math.Sqrt(width)) + (col / (int)Math.Sqrt(width));
            return pos;
        }
    }
}
