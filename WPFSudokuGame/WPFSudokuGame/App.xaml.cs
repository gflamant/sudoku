﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using SplashScreen = WPFSudokuGame.Startup.SplashScreen;

namespace WPFSudokuGame
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static SudokuViewModel ViewModelSudoku { get; set; }

        static App()
        {
            ViewModelSudoku = new SudokuViewModel();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            SplashScreen splashScreen = new SplashScreen();
            splashScreen.Show();

            //use during development to generate image and embed it in application
            //splashScreen.Capture(WPF_DynamicSplashScreen.Properties.Resources.StaticSplashScreen.ToString());

            var startupTask = new Task(() =>
            {
            });

            //when plugin loading finished, show main window
            startupTask.ContinueWith(t =>
            {
                MainWindow mainWindow = new MainWindow();

                //when main windows is loaded close splash screen
                mainWindow.Loaded += (sender, args) => splashScreen.Close();

                //set application main window;
                this.MainWindow = mainWindow;

                //and finally show it
                mainWindow.Show();
            }, TaskScheduler.FromCurrentSynchronizationContext());

            startupTask.Start();
        }
    }
}
