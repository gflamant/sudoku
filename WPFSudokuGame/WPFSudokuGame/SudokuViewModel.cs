﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace WPFSudokuGame
{
    public class SudokuViewModel : INotifyPropertyChanged
    {
        private string nomApplication;
        ImageSource _yourImage;
        ObservableCollection<Grille> grilleList, grilleListFiltered;
        Grille grilleSelect;
        int generatedGrille = 1;

        //Nombre de grille généré (sert pour le nom des grilles généré, pour l'incrémentation)
        public int GeneratedGrille
        {
            get { return generatedGrille; }
            set { generatedGrille = value; }
        }

        //Grille sélectionnée
        public Grille GrilleSelect
        {
            get { return grilleSelect; }
            set { grilleSelect = value; 
                //Notify le changement d'une valeur
                InvokePropertyChanged("grilleSelect"); }
        }
    
        public SudokuViewModel()
        {
            NomApplication = "Sudoku Game 3";

            //Image au dessus de la grille
            var bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(WPFSudokuGame.Properties.Resources.sudokugame.GetHbitmap(),
                                  IntPtr.Zero,
                                  Int32Rect.Empty,
                                  BitmapSizeOptions.FromEmptyOptions());

            _yourImage = (ImageSource)bitmapSource;

            GrilleList = new ObservableCollection<Grille>();
        }
        
        //Nom de l'application
        public string NomApplication
        {
            get { return nomApplication; }
            set { nomApplication = value; }
        }

        public ImageSource YourImage
        {
            get { return _yourImage; }
            set
            {
                _yourImage = value;
            }
        }

        //Collection de toutes les grilles
        public ObservableCollection<Grille> GrilleList { get { return grilleList; } set { grilleList = value; GrilleListFiltered = grilleList; } }

        //Collection des grilles, filtrés
        public ObservableCollection<Grille> GrilleListFiltered { get { return grilleListFiltered; } set { grilleListFiltered = value; InvokePropertyChanged("GrilleListFiltered"); } }

        //Permet d'accéder à l'icone de refus d'une grille
        public ImageSource RefuseImage
        {
            get
            {
                var bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(WPFSudokuGame.Properties.Resources.refused_icon.GetHbitmap(),
                                  IntPtr.Zero,
                                  Int32Rect.Empty,
                                  BitmapSizeOptions.FromEmptyOptions());


                return (ImageSource)bitmapSource;
            }
        }

        //Permet d'accéder à l'icone de Acceptation d'une grille
        public ImageSource AcceptImage
        {
            get
            {
                var bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(WPFSudokuGame.Properties.Resources.valid_icon.GetHbitmap(),
                                  IntPtr.Zero,
                                  Int32Rect.Empty,
                                  BitmapSizeOptions.FromEmptyOptions());


                return (ImageSource)bitmapSource;
            }
        }

        //Permet d'accéder à l'icone de commencement d'une grille
        public ImageSource ModifyImage
        {
            get
            {
                var bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(WPFSudokuGame.Properties.Resources.modify_icon.GetHbitmap(),
                                  IntPtr.Zero,
                                  Int32Rect.Empty,
                                  BitmapSizeOptions.FromEmptyOptions());


                return (ImageSource)bitmapSource;
            }
        }

        //Permet un refresh des valeurs dans l'UI
        private void InvokePropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
}
