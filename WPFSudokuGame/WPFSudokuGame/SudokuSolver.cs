﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFSudokuGame
{

    /// <summary>
    /// Grille de résolution des grilles
    /// 
    /// 9*9 uniquement
    /// </summary>
    class SudokuSolver
    {
        //Vérification backtracking pour grille 9*9 seulement.
        //Utilisation des méthodes recursives

        /// <summary>
        /// Méthode de vérification des lignes
        /// </summary>
        /// <param name="k">Valeur à vérifier</param>
        /// <param name="sudokuToCheck">Sudoku à vérifier</param>
        /// <param name="i">Ligne à vérifier</param>
        /// <returns></returns>
        public bool checkLine(char k, Grille sudokuToCheck, int i)
        {
            for (int j = 0; j < 9; j++)
            {
                if (sudokuToCheck.Tab[i, j].ValeurCase == k)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Méthode de vérification des colonnes
        /// </summary>
        /// <param name="k">Valeur à vérifier</param>
        /// <param name="sudokuToCheck">Sudoku à vérifier</param>
        /// <param name="j">Colonne</param>
        /// <returns></returns>
        public bool checkColumn(char k, Grille sudokuToCheck, int j)
        {
            for (int i = 0; i < 9; i++)
            {
                if (sudokuToCheck.Tab[i, j].ValeurCase == k)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Vérification d'un carré
        /// </summary>
        /// <param name="k">La valeur à vérifier</param>
        /// <param name="sudokuToCheck">La grille à vérifier</param>
        /// <param name="i">Ligne</param>
        /// <param name="j">Colonne</param>
        /// <returns></returns>
        public bool checkArea (char k, Grille sudokuToCheck, int i, int j)
        {
            int _i = i - (i % 3), _j = j - (j % 3);
            for (i=_i; i < _i+3; i++)
                for (j=_j; j < _j+3; j++)
                    if (sudokuToCheck.Tab[i,j].ValeurCase == k)
                        return false;
            return true;
        }

        /// <summary>
        /// Méthode d'appel principal du Sudoku Solver
        /// </summary>
        /// <param name="sudokuToCheck">Sudoku à vérifier</param>
        /// <param name="position">La position à partir du quel on commence à résoudre.
        /// Pour un premier lancement, lancer à 0
        /// </param>
        /// <returns></returns>
        public bool estValide(Grille sudokuToCheck, int position)
        {
            //Toutes les cellules ont été vérifiées
            if (position == 9 * 9)
            {
                sudokuToCheck.IsValid = true;
                sudokuToCheck.setImageToValid();
                return true;
            }

            //Détermination de la position actuelle
            int i = position/9, j = position%9;

            //Si c'est une cellule vide on la vérifie
            if (sudokuToCheck.Tab[i,j].ValeurCase != '.')
                return estValide(sudokuToCheck, position+1);

            for (int k=1; k <= 9; k++)
            {
                if (checkLine(Char.Parse(k.ToString()),sudokuToCheck,i) && checkColumn(Char.Parse(k.ToString()),sudokuToCheck,j) && checkArea(Char.Parse(k.ToString()),sudokuToCheck,i,j))
                {
                    //Si la valeur est sur, on l'ajoute
                    sudokuToCheck.Tab[i,j].ValeurCase = Char.Parse(k.ToString());

                    if (estValide(sudokuToCheck, position + 1))
                    {
                        sudokuToCheck.IsStarted = true;
                        foreach (var item in sudokuToCheck.Tab)
                        {
                            item.isValid = true;
                        }
                        return true;
                    }
                }
            }
            sudokuToCheck.Tab[i,j].ValeurCase = '.';

            return false;
        }
    }
}
