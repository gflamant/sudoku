﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFSudokuGame
{
    public class Square
    {
        public Dictionary<string, Case> Cases { get; set; }
        public int SizeOfSquare { get; set; }

        public Square(int sizeOfQuare)
        {
            this.SizeOfSquare = sizeOfQuare;
            this.Cases = new Dictionary<string, Case>();
        }
    }
}
