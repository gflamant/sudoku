﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.VisualBasic;
using System.ComponentModel;
using System.Diagnostics;

namespace WPFSudokuGame
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = App.ViewModelSudoku;
            Thread.Sleep(3000);
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FrontGrille.Children.Clear();
            FrontGrille.RowDefinitions.Clear();
            FrontGrille.ColumnDefinitions.Clear();
            
            Grille g = App.ViewModelSudoku.GrilleSelect;
            if (g != null)
            {
                for (int i = 0; i < g.Taille; i++)
                {
                    FrontGrille.RowDefinitions.Add(new RowDefinition());
                    FrontGrille.ColumnDefinitions.Add(new ColumnDefinition());
                }

                #region 16
                if (g.Taille == 16)
                {
                    int compteur = 0, compteur2 = 0;
                    bool reverse = false;
                    int k = 0;
                    for (int i = 0; i < g.Taille; i++)
                    {
                        for (int j = 0; j < g.Taille; j++)
                        {
                            compteur++;
                            if (reverse == false)
                            {
                                if (compteur < Math.Sqrt(g.Taille))
                                {
                                    k = 0;
                                    compteur = 0;
                                }
                                else
                                    k = 1;
                            }
                            else
                            {
                                if (compteur < Math.Sqrt(g.Taille))
                                {
                                    k = 1;
                                    compteur = 0;
                                }
                                else
                                    k = 0;
                            }
                            FrameworkElement elem = CreerCaseDeGrid(g, i, j, k);
                            FrontGrille.Children.Add(elem);
                        }
                        compteur2++;
                        if (compteur2 < Math.Sqrt(g.Taille))
                        {
                            reverse = false;
                        }
                        else
                        {
                            reverse = true;
                            if (compteur2 > Math.Sqrt(g.Taille) + Math.Sqrt(g.Taille) - 1)
                            {
                                compteur2 = 0;
                                reverse = false;
                            }
                        }
                    }
                }
                #endregion
                else
                {
                    int compteur = 0;
                    int k = 0;
                    for (int i = 0; i < g.Taille; i++)
                    {
                        for (int j = 0; j < g.Taille; j++)
                        {
                            compteur++;
                            if (compteur < Math.Sqrt(g.Taille))
                            {
                                k = 0;
                                compteur = 0;
                            }
                            else
                                k = 1;
                            FrameworkElement elem = CreerCaseDeGrid(g, i, j, k);
                            FrontGrille.Children.Add(elem);
                        }
                    }
                }

                if (g.IsValid)
                    gridManagement.IsEnabled = false;
                else
                    gridManagement.IsEnabled = true;

                if (g.Taille != 9 || g.isCompleted == true)
                {
                    btnCorrection.IsEnabled = false;
                    btnCorrection.ToolTip = "Disponible pour les Sudoku format 9x9 uniquement et les grilles ";
                }
                else
                {
                    btnCorrection.IsEnabled = true;
                    btnCorrection.ToolTip = "Corriger la grille de base.";
                }
            }
        }

        private static FrameworkElement CreerCaseDeGrid(Grille g, int i, int j, int k)
        {
            FrameworkElement elementGraphique;
            
 //           char c = g.Tab[i, j].Valeur;


            Button b = new Button();

            Binding myBinding = new Binding("ValeurCase");
            myBinding.Source = g.Tab[i, j];
            // myText is an instance of TextBlock
            b.SetBinding(Button.ContentProperty, myBinding);

            if (squareNumber(i, j, g.Taille) % 2 == k)
            {
                if (g.Tab[i, j].isValid)
                {
                    b.Background = new SolidColorBrush(Colors.Gainsboro);
                    b.Focusable = false;
                }
                else
                {
                    b.Background = new SolidColorBrush(Colors.Orange);
                    b.Focusable = true;
                    b.Click += new RoutedEventHandler(MyButtonHandler);
                    b.KeyUp += new KeyEventHandler(MyKeyEventHandler);
                }
            }
            else
            {
                if (g.Tab[i, j].isValid)
                {
                    b.Background = new SolidColorBrush(Colors.GhostWhite);
                    b.Focusable = false;
                }
                else
                {
                    b.Background = new SolidColorBrush(Colors.Orange);
                    b.Focusable = true;
                    b.Click += new RoutedEventHandler(MyButtonHandler);
                    b.KeyUp += new KeyEventHandler(MyKeyEventHandler);
                }
            }

            if (g.Tab[i, j].ValeurCase == '.')
            {
                b.Click += new RoutedEventHandler(MyButtonHandler);
                b.KeyUp += new KeyEventHandler(MyKeyEventHandler);
                b.Focusable = true;
            }

            b.Foreground = new SolidColorBrush(Colors.Indigo);
            elementGraphique = b;
            
            Grid.SetRow(elementGraphique, i);
            Grid.SetColumn(elementGraphique, j);
            return elementGraphique;
        }

        /// <summary>
        /// Réceptionne les touches du clavier, seul les Lettres / Chiffres sont comptabilisés
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void MyKeyEventHandler(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Down && e.Key != Key.Left && e.Key != Key.Right && e.Key != Key.Up)
            {
                Button btn = (Button)sender;
                char result = (char)btn.Content;
                List<Key> NumKeys = new List<Key>();
                NumKeys.Add(Key.NumPad0);
                NumKeys.Add(Key.NumPad1);
                NumKeys.Add(Key.NumPad2);
                NumKeys.Add(Key.NumPad3);
                NumKeys.Add(Key.NumPad4);
                NumKeys.Add(Key.NumPad5);
                NumKeys.Add(Key.NumPad6);
                NumKeys.Add(Key.NumPad7);
                NumKeys.Add(Key.NumPad8);
                NumKeys.Add(Key.NumPad9);

                string pattern = @"[A-z]";

                if (NumKeys.Contains(e.Key))
                {
                    result = e.Key.ToString()[6];
                }
                else
                    if (e.Key.ToString().Count() == 1 && System.Text.RegularExpressions.Regex.IsMatch(e.Key.ToString(), pattern, System.Text.RegularExpressions.RegexOptions.IgnoreCase))
                    {
                        Char.TryParse(e.Key.ToString(), out result);
                    }

                modifyCaseValue(App.ViewModelSudoku.GrilleSelect, Grid.GetRow(btn), Grid.GetColumn(btn), result);
            }
        }

        //Permet de déterminer les carrés
        private static int squareNumber(int row, int col, int width)
        {
            int pos = ((row / (int)Math.Sqrt(width)) * (int)Math.Sqrt(width)) + (col / (int)Math.Sqrt(width));
            return pos;
        }

        //Ouverture chargement d'un fichier via le menu "charger..."
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".png";
            dlg.Filter = "SUDOKU (*.sud)|*.sud";

            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                Console.WriteLine(filename);
                loadFile(filename);
            }

            if (App.ViewModelSudoku.GrilleList.Count > 0 && App.ViewModelSudoku.GrilleSelect == null)
            {
                lstxGrille.SelectedIndex = 0;
                gridManagement.IsEnabled = true;
            }
        }

        //En cliquant sur une case, permet de changer la valeur
        static void MyButtonHandler(object sender, RoutedEventArgs e) {
            Button btn = (Button)sender;
            var dialog = new MyDialog();
            dialog.ResponseTextBox.Focus();
            if (dialog.ShowDialog() == true)
            {
                btn.Background = new SolidColorBrush(Colors.Yellow);
                char response;
                Char.TryParse(dialog.ResponseText, out response);
                modifyCaseValue(App.ViewModelSudoku.GrilleSelect, Grid.GetRow(btn), Grid.GetColumn(btn), response);
            }
        }

        /// <summary>
        /// Chargement d'un fichier .SUD
        /// </summary>
        /// <param name="fileName"></param>
        static void loadFile(string fileName)
        {
            new SudokuManager(fileName);
        }

        /// <summary>
        /// Modification de la valeur de la case
        /// </summary>
        /// <param name="g">Grille sélectionnée</param>
        /// <param name="row">Ligne de la case</param>
        /// <param name="col">Colonne de la case</param>
        /// <param name="value">Nouvelle valeur à affecter</param>
        static void modifyCaseValue(Grille g, int row, int col, char value)
        {
            new SudokuManager().modifyCaseValue(g, row, col, value);
        }

        /// <summary>
        /// Lancer la vérification de la grille
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnValidateModif_Click(object sender, RoutedEventArgs e)
        {
            if (App.ViewModelSudoku.GrilleSelect != null)
            {
                var a = new SudokuManager();
                a.CheckGrille(App.ViewModelSudoku.GrilleSelect);
            }

            if (App.ViewModelSudoku.GrilleSelect.IsValid)
                gridManagement.IsEnabled = false;
        }

        /// <summary>
        /// Récupère les fichier .SUD en drag&drop
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstxGrille_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                // Note that you can have more than one file.
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);


                foreach (string file in files)
                {
                    if(System.IO.Path.GetExtension(file).ToLower() == ".sud")
                        loadFile(file);
                }
            }
        }

        /// <summary>
        /// Réinitialise le tableau à sa valeur original
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            new SudokuManager().resetGrid(App.ViewModelSudoku.GrilleSelect);
        }

        /// <summary>
        /// Filtre
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxFilter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (App.ViewModelSudoku.GrilleList.Count() > 0)
            {
                var index = cbxFilter.SelectedIndex;

                switch (index)
                {
                    case 0:
                        new SudokuManager().FilterGetAll();
                        if (App.ViewModelSudoku.GrilleListFiltered.Count() == 0)
                            gridManagement.IsEnabled = false;
                        break;

                    case 1:
                        new SudokuManager().FilterGetModified();
                        if (App.ViewModelSudoku.GrilleListFiltered.Count() == 0)
                            gridManagement.IsEnabled = false;
                        break;

                    case 2:
                        new SudokuManager().FilterGetRefused();
                        if (App.ViewModelSudoku.GrilleListFiltered.Count() == 0)
                            gridManagement.IsEnabled = false;
                        break;

                    case 3:
                        new SudokuManager().FilterGetAccepted();
                        if (App.ViewModelSudoku.GrilleListFiltered.Count() == 0)
                            gridManagement.IsEnabled = false;
                        break;

                    default:
                        new SudokuManager().FilterGetAll();
                        if (App.ViewModelSudoku.GrilleListFiltered.Count() == 0)
                            gridManagement.IsEnabled = false;
                        break;
                }
            }
        }
        
        /// <summary>
        /// Correction de la grille
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCorrection_Click(object sender, RoutedEventArgs e)
        {
            new SudokuManager().resetGrid(App.ViewModelSudoku.GrilleSelect);

            Stopwatch stopwatch = new Stopwatch();
            App.ViewModelSudoku.GrilleSelect.Stopwatch = stopwatch;
            stopwatch.Start();

            new SudokuSolver().estValide(App.ViewModelSudoku.GrilleSelect, 0);

            stopwatch.Stop();
            App.ViewModelSudoku.GrilleSelect.StopwatchEllapsed = stopwatch.Elapsed.ToString();

            if (App.ViewModelSudoku.GrilleSelect.IsValid)
            {
                //Désactivation des options
                gridManagement.IsEnabled = false;
            }
        }

        //Résolution de toutes les grilles 9*9
        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            if (App.ViewModelSudoku.GrilleList.Count > 0)
            {
                pbStatus.Visibility = Visibility.Visible;
                txtbProgess.Visibility = Visibility.Visible;

                BackgroundWorker bw = new BackgroundWorker();

                List<Grille> grilleList = new List<Grille>();

                foreach (var g in App.ViewModelSudoku.GrilleList)
                {
                    if (g.Taille == 9)
                        grilleList.Add(g);
                }
                pbStatus.Maximum = grilleList.Count();
                
                bw.WorkerReportsProgress = true;

                //A exécuter en background
                bw.DoWork += new DoWorkEventHandler(
                delegate(object o, DoWorkEventArgs args)
                {
                    BackgroundWorker b = o as BackgroundWorker;
                    int i = 0;
                    try
                    {
                        foreach (var item in grilleList)
                        {
                            b.ReportProgress(i);
                            Application.Current.Dispatcher.Invoke(new Action(() =>
                                {
                                    Stopwatch stopwatch = new Stopwatch();
                                    item.Stopwatch = stopwatch;
                                    stopwatch.Start();
                                    new SudokuSolver().estValide(item, 0);
                                    stopwatch.Stop();
                                    item.StopwatchEllapsed = stopwatch.Elapsed.ToString();
                                    i++;
                                    b.ReportProgress(i);
                                }));
                            i++;
                        }
                    }
                    catch (Exception x)
                    {
                        Console.WriteLine(x.Message);
                    }
                });

                bw.ProgressChanged += new ProgressChangedEventHandler(
                delegate(object o, ProgressChangedEventArgs args)
                {
                    pbStatus.Value = args.ProgressPercentage;
                    txtbProgess.Text = string.Format("{0}/{1} résolus", args.ProgressPercentage, grilleList.Count());
                });

                bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(
                delegate(object o, RunWorkerCompletedEventArgs args)
                {
                    txtbProgess.Text = "Finished!";
                    pbStatus.Visibility = Visibility.Hidden;
                    txtbProgess.Visibility = Visibility.Hidden;

                    int si = lstxGrille.SelectedIndex;
                    lstxGrille.SelectedIndex = -1;
                    lstxGrille.SelectedIndex = si;
                });

                bw.RunWorkerAsync();
            }
        }
        
        /// <summary>
        /// Event génération des sudokus 9*9
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mitemGenSud9_Click(object sender, RoutedEventArgs e)
        {
            var grilleToAdd = new SudokuGenerator().generateSudokuFormat9();
            if (grilleToAdd != null)
            {
                App.ViewModelSudoku.GrilleList.Add(grilleToAdd);
                lstxGrille.SelectedIndex = App.ViewModelSudoku.GrilleList.Count() - 1;
            }
        }
        
        /// <summary>
        /// Event génération des sudokus 25*25
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mitemGenSud25_Click(object sender, RoutedEventArgs e)
        {
            var grilleToAdd = new SudokuGenerator().generateSudokuFormat25();
            if (grilleToAdd != null)
            {
                App.ViewModelSudoku.GrilleList.Add(grilleToAdd);
                lstxGrille.SelectedIndex = App.ViewModelSudoku.GrilleList.Count() - 1;
            }
        }

        /// <summary>
        /// Génération des sudokus 16*16
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mitemGenSud16_Click(object sender, RoutedEventArgs e)
        {
            var grilleToAdd = new SudokuGenerator().generateSudokuFormat16();
            if (grilleToAdd != null)
            {
                App.ViewModelSudoku.GrilleList.Add(grilleToAdd);
                lstxGrille.SelectedIndex = App.ViewModelSudoku.GrilleList.Count() - 1;
            }
        }

        /// <summary>
        /// Event d'export des sudokus résolus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportSolvedSudoku_Click(object sender, RoutedEventArgs e)
        {
            new SudokuManager().exportSolvedSudoku();
        }
    }
}
