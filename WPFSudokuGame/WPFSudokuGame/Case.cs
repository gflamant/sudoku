﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFSudokuGame
{
    public class Case : INotifyPropertyChanged
    {
        char valeurCase;

        public char ValeurCase
        {
            get { return valeurCase; }
            set { valeurCase = value; InvokePropertyChanged("ValeurCase"); }
        }
        
        public string Symboles { get; set; }

        public int SquareNumber { get; set; }

        public bool isValid { get; set; }


        public Case(char val, string Symboles)
        {
            this.ValeurCase = val;
            this.Symboles = Symboles;
            this.isValid = true;
        }

        public object Hypoteses { get; set; }

        private void InvokePropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
