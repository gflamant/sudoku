﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace WPFSudokuGame.Startup
{
    /// <summary>
    /// Logique d'interaction pour SplashScreen.xaml
    /// </summary>
    public partial class SplashScreen
    {
        public static readonly DependencyProperty AvailablePluginsProperty =
            DependencyProperty.Register("AvailablePlugins", typeof(IEnumerable<string>), typeof(SplashScreen),
                                        new UIPropertyMetadata(null));

        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register("Message", typeof(string), typeof(SplashScreen),
                                        new UIPropertyMetadata(null, OnMessageChanged));


        public static readonly DependencyProperty LicenseeProperty =
            DependencyProperty.Register("Licensee", typeof(string), typeof(SplashScreen), new UIPropertyMetadata(null));


        public SplashScreen()
        {
            this.InitializeComponent();

            var bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(WPFSudokuGame.Properties.Resources.sudokugame2.GetHbitmap(),
                                  IntPtr.Zero,
                                  Int32Rect.Empty,
                                  BitmapSizeOptions.FromEmptyOptions());

            ImageBrush img = new ImageBrush(bitmapSource);

            gridSplashScreen.Background = img;
        }

        public string Licensee
        {
            get { return (string)this.GetValue(LicenseeProperty); }
            set { this.SetValue(LicenseeProperty, value); }
        }


        public IEnumerable<string> AvailablePlugins
        {
            get { return (IEnumerable<string>)this.GetValue(AvailablePluginsProperty); }
            set { this.SetValue(AvailablePluginsProperty, value); }
        }


        public string Message
        {
            get { return (string)this.GetValue(MessageProperty); }
            set { this.SetValue(MessageProperty, value); }
        }


        public event EventHandler MessageChanged;

        private void RaiseMessageChanged(EventArgs e)
        {
            EventHandler handler = this.MessageChanged;
            if (handler != null) handler(this, e);
        }

        private static void OnMessageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SplashScreen splashScreen = (SplashScreen)d;
            splashScreen.RaiseMessageChanged(EventArgs.Empty);
            //splashScreen.messageTextBlock.Text = splashScreen.Message;
        }
    }

}
