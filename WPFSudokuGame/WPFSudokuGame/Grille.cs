﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace WPFSudokuGame
{
    public class Grille : INotifyPropertyChanged
    {
        Stopwatch stopwatch;
        string stopwatchEllapsed;

        public string StopwatchEllapsed
        {
            get {
                if (stopwatch == null) 
                    return "00:00:00";
                else 
                    return stopwatchEllapsed; 
            }

            set { stopwatchEllapsed = value; InvokePropertyChanged("StopwatchEllapsed"); }
        }
        ImageSource statusGrilleImage;
        bool isValid;
        Case[,] tab;

        public Case[,] Tab
        {
            get { return tab; }
            set { tab = value; createOriginalCopyOf(tab);}
        }

        public Stopwatch Stopwatch
        {
            get { return stopwatch; }
            set { stopwatch = value;}
        }
        
        public string Nom { get; set; }
        public string Date { get; set; }
        public string Symboles { get; set; }
        public int Taille { get { return Symboles.Length; } }

        public bool IsValid
        {
            get { return isValid; }
            set { isValid = value; InvokePropertyChanged("Validity"); if (IsValid) setImageToValid(); }
        }

        public ImageSource StatusGrilleImage
        {
            get { return statusGrilleImage; }
            set { statusGrilleImage = value; InvokePropertyChanged("StatusGrilleImage"); }
        }

        public bool isCompleted { get; set; }

        private bool isStarted { get; set; }

        public bool IsStarted { get { return isStarted; } set { isStarted = value; if (!IsStarted) setImageToModify(); } }

        public string Validity
        {
            get
            {
                if (this.isValid)
                    return "Valide";
                else
                    return "Invalide";
            }
        }

        //Taille du Sudoku, sert pour l'affichage
        public string DisplayTaille { get { return Symboles.Length + "x" + Symboles.Length; } }

        //Grille de base, la grille chargée.
        //Sert pour la réinitialisation
        public Case[,] UnchangedTab { get; set; }

        private void InvokePropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        
        //Liste des cases à corriger
        public Dictionary<string, Case> CaseToCorrect { get; set; }

        //Liste des carrés
        public Dictionary<int, Square> Squares { get; set; }

        public Grille()
        {
            this.Squares = new Dictionary<int, Square>();
        }

        public void setImageToValid()
        {
            StatusGrilleImage = App.ViewModelSudoku.AcceptImage;
        }

        public void setImageToRefused()
        {
            StatusGrilleImage = App.ViewModelSudoku.RefuseImage;
        }

        public void setImageToModify()
        {
            StatusGrilleImage = App.ViewModelSudoku.ModifyImage;
        }

        /// <summary>
        /// Création d'un array de case, copie par valeur et non par référence
        /// </summary>
        /// <param name="arrayToCopy"></param>
        private void createOriginalCopyOf(Case[,] arrayToCopy)
        {
            this.UnchangedTab = new Case[(int)Math.Sqrt(arrayToCopy.Length),(int)Math.Sqrt(arrayToCopy.Length)];
            for (int i = 0; i < (int)Math.Sqrt(arrayToCopy.Length); i++)
            {
                for (int j = 0; j < (int)Math.Sqrt(arrayToCopy.Length); j++)
                {
                    this.UnchangedTab[i, j] = new Case(arrayToCopy[i, j].ValeurCase, arrayToCopy[i, j].Symboles);
                }
            }
        }

        
    }
}
