﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WPFSudokuGame
{
    class SudokuManager
    {
        
        /// <summary>
        /// Lecture du fichier SUD, lancement de l'initialisation des grilles
        /// </summary>
        /// <param name="path">Chemin du fichier SUD</param>
        public SudokuManager(string path)
        {
            launch(path);
        }

        public SudokuManager()
        {}

        public void launch(string filePath)
        {
            //Récupération de la référence de la GrilleList (Observable) pour ajouter les objets
            //Visible dans l'UI
            var grilles = App.ViewModelSudoku.GrilleList;

            #region Variables
            //Valeur d'initialisations
            Grille grilleToAdd = new Grille();
            string nameGrille = "DEFAULT_NAME";
            DateTime sudokuDate = DateTime.Now;
            string formatGrille = "DEFAULT_FORMAT";
            string unformattedGrille = "";
            Case[,] tmpGrille = null;
            bool fstRunning = true, errorFormat = false, HaveToBeCompleted = false;

            //Pattern du délimiteur
            string pattern = @"//-{1,}";


            // nbChar = nombre de caractères dans la ligne
            int i = 0, nbCharInLine = 0, l, itterationOfLine = 0;

            #endregion


            //Chemin du fichier
            try
            {
                string[] lines = File.ReadAllLines(filePath);
                if (lines.Length != 0)
                {
                    foreach (string line in lines)
                    {
                        i++;
                        l = 0;
                        //Récupération des attributs de description de la grille
                        if (i == 2 && line != "")
                            nameGrille = line;
                        else if (i == 3 && line != "")
                            DateTime.TryParse(line, out sudokuDate);
                        else if (i == 4 && line != "")
                        {
                            formatGrille = line;
                            nbCharInLine = line.ToCharArray().Count();
                            tmpGrille = new Case[nbCharInLine, nbCharInLine];
                        }

                        //Détection du délimiteur, reinit des vars, création de la grille si pas d'erreur
                        else if (System.Text.RegularExpressions.Regex.IsMatch(line, pattern, System.Text.RegularExpressions.RegexOptions.IgnoreCase) && !fstRunning || line == "" && !fstRunning)
                        {
                            if (!errorFormat && itterationOfLine == formatGrille.Count())
                            {
                                grilleToAdd.Nom = nameGrille;
                                grilleToAdd.Date = sudokuDate.ToString();
                                grilleToAdd.Symboles = formatGrille;
                                grilleToAdd.Tab = tmpGrille;
                                grilleToAdd.isCompleted = true;
                                if (HaveToBeCompleted)
                                {
                                    grilleToAdd.setImageToModify();
                                    grilleToAdd.isCompleted = false;
                                    grilleToAdd.IsStarted = false;
                                }
                                if (!HaveToBeCompleted)
                                    CheckGrille(grilleToAdd);
                                grilles.Add(grilleToAdd);
                            }
                            grilleToAdd = new Grille();
                            nameGrille = "DEFAULT_NAME";
                            sudokuDate = DateTime.Now;
                            formatGrille = "DEFAULT_FORMAT";
                            i = 1;
                            nbCharInLine = 0;
                            l = 0;
                            itterationOfLine = 0;
                            unformattedGrille = "";
                            HaveToBeCompleted = false;
                            errorFormat = false;
                        }
                        //Traitement de la grille
                        else if (i > 4 && i < nbCharInLine + 5)
                        {
                            unformattedGrille += line;
                            try
                            {
                                foreach (char cValue in line)
                                {
                                    //Console.Write(cValue + " ");
                                    if (cValue == '.')
                                    {
                                        HaveToBeCompleted = true;
                                    }

                                    Case caseToAdd = new Case(cValue, formatGrille);
                                    tmpGrille[i - 5, l] = caseToAdd;

                                    //Si la grille contient déjà le carré X (déterminé par la méthode square number) on ajoute la case dedans
                                    if (grilleToAdd.Squares.ContainsKey(squareNumber(i - 5, l, formatGrille.Count())))
                                    {
                                        Square square = grilleToAdd.Squares[squareNumber(i - 5, l, formatGrille.Count())];
                                        caseToAdd.SquareNumber = squareNumber(i - 5, l, formatGrille.Count());
                                        square.Cases.Add((i - 5).ToString() + l.ToString(), caseToAdd);
                                    }
                                    //Sinon on créé le carré
                                    else
                                    {
                                        Square square = new Square(formatGrille.Count());
                                        caseToAdd.SquareNumber = squareNumber(i - 5, l, formatGrille.Count());
                                        square.Cases.Add((i - 5).ToString() + l.ToString(), caseToAdd);
                                        grilleToAdd.Squares.Add(squareNumber(i - 5, l, formatGrille.Count()), square);
                                    }
                                    l++;
                                }
                                itterationOfLine++;
                            }
                            //Si la ligne d'une grille est trop grande
                            catch (IndexOutOfRangeException e)
                            {
                                Console.WriteLine("/!\\Erreur dans le format du fichier/!\\\nErreur : Grille \"" + nameGrille + "\", ligne : " + (i - 4) + "\n");
                                i = nbCharInLine;
                                errorFormat = true;
                            }
                        }
                        //Booléen de premier lancement
                        if (fstRunning)
                        {
                            fstRunning = false;
                        }
                        //Console.WriteLine(i + " : \t" + line);
                    }
                    //Ajout de la dernière grille
                    if (!errorFormat)
                    {
                        grilleToAdd.Nom = nameGrille;
                        grilleToAdd.Date = sudokuDate.ToString();
                        grilleToAdd.Symboles = formatGrille;
                        grilleToAdd.Tab = tmpGrille;
                        grilleToAdd.isCompleted = true;
                        if (HaveToBeCompleted)
                        {
                            grilleToAdd.setImageToModify();
                            grilleToAdd.isCompleted = false;
                            grilleToAdd.IsStarted = false;
                        }
                        if (!HaveToBeCompleted)
                            CheckGrille(grilleToAdd);
                        grilles.Add(grilleToAdd);
                    }
                    
                }
                else
                {
                    MessageBox.Show("Le fichier est vide.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// Modification la valeur des cases (permet un update dynamique des valeur dans la GUI)
        /// </summary>
        /// <param name="g">Grille</param>
        /// <param name="row">Ligne</param>
        /// <param name="col">Colonne</param>
        /// <param name="value">Nouvelle valeur</param>
        public void modifyCaseValue(Grille g, int row, int col, char value)
        {
            //Si la nouvelle valeur fait partie des symboles
            if (g.Tab[row, col].Symboles.Contains(value))
                g.Tab[row, col].ValeurCase = value;
        }

        /// <summary>
        /// Lancement de la vérification de la grille
        /// </summary>
        /// <param name="g"></param>
        public void CheckGrille(Grille g)
        {
            g.IsStarted = true;
                try
                {
                    
                    bool testCol = CheckColonne(g);
                    bool testRow = CheckLigne(g);
                    bool testSquare = checkCarre(g);

                    if (testCol == false || testRow == false || testSquare == false)
                    {
                        g.setImageToRefused();
                        g.IsValid = false;
                    }
                    else
                    {
                        g.setImageToValid();
                        g.IsValid = true;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    g.IsValid = false;
                }
         
        }

        private bool checkCarre(Grille g)
        {
            bool valueToReturn = true;
            foreach (var square in g.Squares.Values)
            {
                foreach (var laCase in square.Cases.Values)
                {
                    if (square.Cases.Count(D => D.Value.ValeurCase.Equals(laCase.ValeurCase)) > 1)
                    {
                        valueToReturn = false;
                        //Dans le carré, on compte le nombre de case ayant la même valeur 
                        var keys = square.Cases.Where(x => x.Value.ValeurCase.Equals(laCase.ValeurCase)).Select(x => x.Value);

                        //Change la valeur des cases pour qu'elles soient signalées comme fausses
                        foreach (var item in keys)
                        {
                            item.isValid = false;
                        }
                    }
                }
            }
            
            return valueToReturn;
        }

        //Vérification de la grille ligne par ligne
        private bool CheckLigne(Grille g)
        {
            bool isValid = true;
            try
            {
                char toCompare;
                for (int colonne = 0; colonne < g.Taille; colonne++)
                {
                    for (int ligne = 0; ligne < g.Taille; ligne++)
                    {
                        toCompare = g.Tab[colonne, ligne].ValeurCase;
                        for (int i = 0; i < g.Taille; i++)
                        {
                            if (i != ligne)
                            {
                                if (g.Tab[colonne, i].ValeurCase == toCompare)
                                {
                                    isValid = false;
                                    g.Tab[colonne, ligne].isValid = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (IndexOutOfRangeException)
            {
                g.IsValid = false;
                g.setImageToRefused();
                return false;
            }
            return isValid;
        }

        //Vérification de la grille colonne par colonne
        private bool CheckColonne(Grille g)
        {
            bool isValid = true;
            try
            {
                char toCompare;
                for (int colonne = 0; colonne < g.Taille; colonne++)
                {
                    for (int ligne = 0; ligne < g.Taille; ligne++)
                    {
                        toCompare = g.Tab[colonne, ligne].ValeurCase;
                        for (int i = 0; i < g.Taille; i++)
                        {
                            if (i != colonne)
                            {
                                if (g.Tab[i, ligne].ValeurCase == toCompare)
                                {
                                    isValid = false;
                                    if (g.Nom == "Test 666")
                                        Console.WriteLine();
                                    g.Tab[colonne, ligne].isValid = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (IndexOutOfRangeException)
            {
                g.IsValid = false;
                g.setImageToRefused();
                return false;
            }
            return isValid;
        }

        /// <summary>
        /// Méthode qui retourne le 
        /// </summary>
        /// <param name="row">Ligne actuelle</param>
        /// <param name="col">Colonne actuelle</param>
        /// <param name="width">Taille d'un carré</param>
        /// <returns></returns>
        private static int squareNumber(int row, int col, int width)
        {
            int pos = ((row / (int)Math.Sqrt(width)) * (int)Math.Sqrt(width)) + (col / (int)Math.Sqrt(width));
            return pos;
        }

        /// <summary>
        /// Réinitialisation de la valeur des cases
        /// </summary>
        /// <param name="g">Grille à réinitialiser</param>
        public void resetGrid(Grille g)
        {
            for (int i = 0; i < (int)Math.Sqrt(g.Tab.Length); i++)
            {
                for (int j = 0; j < (int)Math.Sqrt(g.Tab.Length); j++)
                {
                    g.Tab[i, j].ValeurCase = g.UnchangedTab[i, j].ValeurCase;
                }
            }
        }

        /// <summary>
        /// Retourne la liste de toutes les grilles
        /// </summary>
        public void FilterGetAll()
        {
            App.ViewModelSudoku.GrilleListFiltered = App.ViewModelSudoku.GrilleList;
        }

        /// <summary>
        /// Filtre les grilles et retourne une liste observale de grilles à commencer
        /// </summary>
        public void FilterGetModified()
        {
            ObservableCollection<Grille> obs = new ObservableCollection<Grille>(App.ViewModelSudoku.GrilleList.Where(x=>x.IsStarted.Equals(false)));
            App.ViewModelSudoku.GrilleListFiltered = obs;
        }

        /// <summary>
        /// Filtre les grilles et retourne une liste observale de grilles refusé
        /// </summary>
        public void FilterGetAccepted()
        {
            ObservableCollection<Grille> obs = new ObservableCollection<Grille>(App.ViewModelSudoku.GrilleList.Where(x => x.IsValid.Equals(true)));
            App.ViewModelSudoku.GrilleListFiltered = obs;
        }

        /// <summary>
        /// Filtre les grilles et retourne une liste observale de grilles refusé
        /// </summary>
        public void FilterGetRefused()
        {
            ObservableCollection<Grille> obs = new ObservableCollection<Grille>();
            foreach (var grille in App.ViewModelSudoku.GrilleList)
            {
                if(grille.IsValid == false && grille.IsStarted)
                {
                    obs.Add(grille);
                }
            }
            App.ViewModelSudoku.GrilleListFiltered = obs;
        }

        /// <summary>
        /// Méthode d'export des sudokus résolu
        /// </summary>
        public void exportSolvedSudoku()
        {
            ObservableCollection<Grille> obs = new ObservableCollection<Grille>(App.ViewModelSudoku.GrilleList.Where(x => x.IsValid.Equals(true)));

            //Si le nom de grille est supérieur à 0
            if (obs.Count > 0)
            {
                string fileToExport = "";

                foreach (var grille in obs)
                {
                    fileToExport += "//---------------------------" + Environment.NewLine;
                    for (int l = 0; l < grille.Taille+3; l++)
                    {
                        if (l == 0)
                            fileToExport += grille.Nom + Environment.NewLine;
                        if (l == 1)
                            fileToExport += grille.Date + Environment.NewLine;
                        if (l == 2)
                            fileToExport += grille.Symboles + Environment.NewLine;
                        if (l >= 3 && l < grille.Taille+3)
                        {
                            for (int j = 0; j < grille.Taille; j++)
                            {
                                fileToExport += grille.Tab[l-3, j].ValeurCase;
                            }
                            fileToExport += Environment.NewLine;
                            Console.WriteLine();
                        }
                    } 
                }

                string datePatt = @"ddMMyyyyhhmm";
                SaveFileDialog dialog = new SaveFileDialog()
                {
                    Filter = "SUDOKU(*.sud)|*.sud|Fichier texte(*.txt)|*.txt"
                };
                dialog.FileName = "SudokuGameExport" + DateTime.Now.ToString(datePatt) + ".sud";

                if (dialog.ShowDialog() == true)
                {
                    File.WriteAllText(dialog.FileName, fileToExport);
                }
            }
            else
            {
                MessageBox.Show("Aucune grilles à exporter.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}
